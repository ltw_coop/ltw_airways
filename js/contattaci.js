
function validaNomeCognome() {
    if (checkString(document.contact.nomcog.value)) return true;
    else {
        msg("Dati non validi");
        return false;
    }
}

function validaEmail() {
    if (checkEmail(document.contact.email.value)) return true;
    else {
        msg("Email non valida");
        return false;
    }
}


function validaTelefono() {
    if (checkPhoneNumber(document.contact.telefono.value)) return true;
    else {
        msg("Numero non valido");
        return false;
    }
}

function validaMessaggio() {
    if (checkString(document.contact.messaggio.value)) return true;
    else {
        msg("Messaggio non valido");
        return false;
    }
}

function totalcheck() {
    if ($("#invalidCheck").prop('checked', true)) {
        $("#sendMessageButton").removeClass("disabled");
    }
    else {
        $("#sendMessageButton").addClass("disabled");
    }

}


function invia() {
    if (validaNomeCognome2() && validaEmail2() && validaTelefono2() && validaMessaggio2()) {
        $("#centralModalSuccess").modal();
    }
    else {
        $("#centralModalWarning").modal();

    };
}

function validaNomeCognome2() {
    return (checkString(document.contact.nomcog.value));
}

function validaEmail2() {
    return (checkEmail(document.contact.email.value)) ;
}


function validaTelefono2() {
    return (checkNumber(document.contact.telefono.value));
}

function validaMessaggio2() {
    return (checkString(document.contact.messaggio.value)); 
}