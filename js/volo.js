var PREZZO_ADULTI = 1;
var PREZZO_TEEN = 0.8;
var PREZZO_BAMBINI = 0.6;
var PREZZO_NEONATI = 0.4;

function getDate(data) {
    var mydate = data.split("-");
    return mydate[2] + "-" + mydate[1] + "-" + mydate[0];
}

function checkVal(valore) {
    if (valore != "") return valore;
    else return 0;
}

function checkRadio1() {   
        $("#buttonVoli1").removeClass("disabled");
}

function checkRadio2() {
        $("#buttonVoli2").removeClass("disabled");
}

function numAdulti(numero) {
    if ((numero && numero != 0)) {
        return " | " + numero + " Adulti";
    } else {
        return "";
    }
}

function numTeen(numero) {
    if (numero && numero != 0) {
        return " | " + numero + " Teen";
    } else {
        return "";
    }
}

function numBimbi(numero) {
    if (numero && numero != 0) {
        return " | " + numero + " Bimbi";
    } else {
        return "";
    }
}

function numNeonati(numero) {
    if (numero && numero != 0) {
        return " | " + numero + " Neonati";
    } else {
        return "";
    }
}

function prezzo(adulti, teen, bambini, neonati) {
    var totale = adulti * PREZZO_ADULTI + teen * PREZZO_TEEN + bambini * PREZZO_BAMBINI + neonati * PREZZO_NEONATI;
    return totale;
}

function mycheckAereoporto(numero) {
    if (checkAereoporti(document.getElementsByName("partenza"+numero)[0].value, document.getElementsByName("destinazione"+numero)[0].value)) {
        return true;
    }
    else {
        blocca(numero);
        msg("Partenza/Arrivo non validi");
        return false;
    }
}

function _mycheckAereoporto(numero) {
    if (checkAereoporti(document.getElementsByName("partenza"+numero)[0].value, document.getElementsByName("destinazione"+numero)[0].value)) {
        return true;
    }
    else {
        blocca(numero);
        return false;
    }
}


function mycheckData1() {
    if (checkDate(document.getElementsByName("tripstart")[0].value)) {
        return true;
    } else {
        blocca(1);
        msg("Data non valida");
        return false;
    }
}

function mycheckData2() {
    var p = document.getElementsByName("tripstart1")[0].value;
    var d = document.getElementsByName("tripstart2")[0].value;
    if (!checkDate(p) || !checkDate(d)) {
        blocca(2);
        msg("Data non valida");
        return false;
    }
    else if (d <= p) {
        blocca(2);
        msg("Data non valida");
        return false;
    }
    else return true;
}


function mycheckPasseggeri(numero) {
    var a=document.getElementsByName("numadulti"+numero)[0].value;
    var t=document.getElementsByName("numteen"+numero)[0].value;
    var b= document.getElementsByName("numbimbi"+numero)[0].value;
    var n=document.getElementsByName("numneonati"+numero)[0].value;
    if(a>10||t>10||b>10||n>10){
        blocca(numero);
        msg("Hai inserito troppi passeggeri");
        return false;
    }
    else if (checkPasseggeri(a,t,b,n)) {
        return true;
    }
    else {
        blocca(numero);
        msg("Hai inserito 0 passeggeri");
        return false;
    }
}


function totalcheck1() {
    if (mycheckAereoporto(1) && mycheckData1() && mycheckPasseggeri(1)) {
        if ($("#button1").hasClass("disabled")) {
            $("#button1").removeClass("disabled");
        }
        return true;
    }
    else {
        blocca(1);
        return false;
    }
}
function totalcheck2() {
    if (mycheckAereoporto(2) && mycheckData2() && mycheckPasseggeri(2)) {
        if ($("#button2").hasClass("disabled")) {
            $("#button2").removeClass("disabled");
        }
        return true;
    }
    else {
        blocca(2);
        return false;
    }
}
