var prezzoAndata = 0;
var prezzoRitorno = 0;

// USATE PER STAMPARE IL PREZZO SCELTO
function mostraRiepilogoAndata() {
    if (prezzoAndata) {
        checkRadio1();
    }
    var u = JSON.parse(sessionStorage.viaggio);
    var s = new String("");
    s += "Hai scelto:" + "<br>"
    s += u[0].partenza + " <i class='fa fa-arrow-right' aria-hidden='true'></i> " + u[0].destinazione + "<br>"
    s += "il giorno: " + getDate(u[0].datapartenza);
    s += "<hr></hr>"
    s += "Adulti: " + checkVal(u[0].numeroadulti) + "<br>" + "Teen: " + checkVal(u[0].numeroteen) + "<br>" + "Bambini: " + checkVal(u[0].numerobimbi) + "<br>" + "Neonati: " + checkVal(u[0].numeroneonati)
    s += "<hr></hr>"
    s += "Numero biglietti: " + numeroBiglietti() + "<br>" + "Totale: € " + prezzoAndata + "<br>";
    document.getElementById("riepilogoPagamento").innerHTML = s;
}

function mostraRiepilogoAndataRitorno() {
    if (prezzoAndata && prezzoRitorno) {
        checkRadio2();
    }
    var u = JSON.parse(sessionStorage.viaggio);
    var s = new String("");
    var v1 = parseInt(prezzoAndata);
    var v2 = parseInt(prezzoRitorno);
    var totalePrezzo = v1 + v2;
    s += "Hai scelto:" + "<br>"
    s += u[0].partenza + " <i class='fa fa-arrow-right' aria-hidden='true'></i> " + u[0].destinazione + "<br>"
    s += "il giorno: " + getDate(u[0].datapartenza);
    s += "<hr></hr>"
    s += "Adulti: " + checkVal(u[0].numeroadulti) + "<br>" + "Teen: " + checkVal(u[0].numeroteen) + "<br>" + "Bambini: " + checkVal(u[0].numerobimbi) + "<br>" + "Neonati: " + checkVal(u[0].numeroneonati)
    s += "<hr></hr>"
    s += "Numero biglietti: " + numeroBiglietti() + "<br>" + "Totale: € " + prezzoAndata + "<br>";
    s += "<hr></hr>"
    s += u[0].destinazione + " <i class='fa fa-arrow-right' aria-hidden='true'></i> " + u[0].partenza + "<br>"
    s += "il giorno: " + getDate(u[0].dataritorno);
    s += "<hr></hr>"
    s += "Adulti: " + checkVal(u[0].numeroadulti) + "<br>" + "Teen: " + checkVal(u[0].numeroteen) + "<br>" + "Bambini: " + checkVal(u[0].numerobimbi) + "<br>" + "Neonati: " + checkVal(u[0].numeroneonati)
    s += "<hr></hr>"
    s += "Numero biglietti: " + numeroBiglietti() + "<br>" + "Totale: € " + prezzoRitorno
    s += "<hr></hr>"
    s += "Costo complessivo: € " + totalePrezzo;
    document.getElementById("riepilogoPagamento").innerHTML = s;
}

function numeroBiglietti() {
    var u = JSON.parse(sessionStorage.viaggio);
    return u[0].numeroadulti * 1 + u[0].numeroteen * 1 + u[0].numerobimbi * 1 + u[0].numeroneonati * 1; 
}

function inizializzaStorageViaggio() {
    if (typeof (sessionStorage.viaggio) == "undefined") {
        sessionStorage.viaggio = "[]";
    }
}

// NON USATA AL MOMENTO
function resetStorageViaggio() {
    sessionStorage.viaggio = "[]";
}

// NON USATA AL MOMENTO
function stampaStorageSemplice() {
    var u = JSON.parse(sessionStorage.viaggio);
    var s = new String("<h3>Stato di sessionStorage:</h3>");
    s += JSON.stringify(u[0]) + "<br/>";
    document.getElementById("vistaStorage").innerHTML = s;
    return true;
}

// PER voloAndata.html
function stampaStorageTabellaAndata() {
    var u = JSON.parse(sessionStorage.viaggio);

    var s = u[0].partenza + " &nbsp; > &nbsp;" + u[0].destinazione + "<br> Data di partenza: " + getDate(u[0].datapartenza) + numAdulti(u[0].numeroadulti) + numTeen(u[0].numeroteen) + numBimbi(u[0].numerobimbi) + numNeonati(u[0].numeroneonati);
    document.getElementById("riepilogoStorage").innerHTML = s;

    //AEREOPORTI
    var s1 = u[0].partenza;
    document.getElementById("partenzaStorage1").innerHTML = s1;
    document.getElementById("partenzaStorage2").innerHTML = s1;
    document.getElementById("partenzaStorage3").innerHTML = s1;
    document.getElementById("partenzaStorage4").innerHTML = s1;

    var s2 = u[0].destinazione;
    document.getElementById("destinazStorage1").innerHTML = s2;
    document.getElementById("destinazStorage2").innerHTML = s2;
    document.getElementById("destinazStorage3").innerHTML = s2;
    document.getElementById("destinazStorage4").innerHTML = s2;

    //COSTO
    var myPrezzo = prezzo(u[0].numeroadulti, u[0].numeroteen, u[0].numerobimbi, u[0].numeroneonati);

    var s3 = "<input type='radio' value='" + (myPrezzo * 10 * 10) + "' id='voloAndata' name='voloAndata' onclick='prezzoAndata=this.value' /> € " + myPrezzo * 10 * 10;
    document.getElementById("economyLightStorage1").innerHTML = s3;
    document.getElementById("economyLightStorage2").innerHTML = s3;
    document.getElementById("economyLightStorage3").innerHTML = s3;
    document.getElementById("economyLightStorage4").innerHTML = s3;

    var s4 = "<input type='radio' value='" + (myPrezzo * 15 * 10) + "' id='voloAndata' name='voloAndata' onclick='prezzoAndata=this.value' /> € " + myPrezzo * 15 * 10;
    document.getElementById("economyClassicStorage1").innerHTML = s4;
    document.getElementById("economyClassicStorage2").innerHTML = s4;
    document.getElementById("economyClassicStorage3").innerHTML = s4;
    document.getElementById("economyClassicStorage4").innerHTML = s4;

    var s5 = "<input type='radio' value='" + (myPrezzo * 20 * 10) + "' id='voloAndata' name='voloAndata' onclick='prezzoAndata=this.value' /> € " + myPrezzo * 20 * 10;
    document.getElementById("businessStorage1").innerHTML = s5;
    document.getElementById("businessStorage2").innerHTML = s5;
    document.getElementById("businessStorage3").innerHTML = s5;
    document.getElementById("businessStorage4").innerHTML = s5;

    return true;
}

// PER voloAndataRitorno.html
function stampaStorageTabellaRitorno() {
    var u = JSON.parse(sessionStorage.viaggio);
    var s = u[0].partenza + " &nbsp; > &nbsp;" + u[0].destinazione + "<br> Data di partenza: " + getDate(u[0].datapartenza) + numAdulti(u[0].numeroadulti) + numTeen(u[0].numeroteen) + numBimbi(u[0].numerobimbi) + numNeonati(u[0].numeroneonati);
    document.getElementById("riepilogoStorage1").innerHTML = s;

    var s1 = u[0].destinazione + " &nbsp; > &nbsp;" + u[0].partenza + "<br> Data di ritorno: " + getDate(u[0].datapartenza) + numAdulti(u[0].numeroadulti) + numTeen(u[0].numeroteen) + numBimbi(u[0].numerobimbi) + numNeonati(u[0].numeroneonati);
    document.getElementById("riepilogoStorage2").innerHTML = s1;

    //AEREOPORTI
    var s2 = u[0].partenza;
    document.getElementById("partenzaStorage1").innerHTML = s2;
    document.getElementById("partenzaStorage2").innerHTML = s2;
    document.getElementById("partenzaStorage3").innerHTML = s2;
    document.getElementById("partenzaStorage4").innerHTML = s2;
    document.getElementById("partenzaStorage5").innerHTML = s2;
    document.getElementById("partenzaStorage6").innerHTML = s2;
    document.getElementById("partenzaStorage7").innerHTML = s2;
    document.getElementById("partenzaStorage8").innerHTML = s2;

    var s3 = u[0].destinazione;
    document.getElementById("destinazStorage1").innerHTML = s3;
    document.getElementById("destinazStorage2").innerHTML = s3;
    document.getElementById("destinazStorage3").innerHTML = s3;
    document.getElementById("destinazStorage4").innerHTML = s3;
    document.getElementById("destinazStorage5").innerHTML = s3;
    document.getElementById("destinazStorage6").innerHTML = s3;
    document.getElementById("destinazStorage7").innerHTML = s3;
    document.getElementById("destinazStorage8").innerHTML = s3;

    //COSTO
    var myPrezzo = prezzo(u[0].numeroadulti, u[0].numeroteen, u[0].numerobimbi, u[0].numeroneonati);

    var s4 = "<input type='radio' value='" + (myPrezzo * 10 * 10) + "' id='voloAndata' name='voloAndata' onclick='prezzoAndata=this.value'/> € " + myPrezzo * 10 * 10;
    document.getElementById("economyLightStorage1").innerHTML = s4;
    document.getElementById("economyLightStorage2").innerHTML = s4;
    document.getElementById("economyLightStorage3").innerHTML = s4;
    document.getElementById("economyLightStorage4").innerHTML = s4;
    var s5 = "<input type='radio' value='" + (myPrezzo * 10 * 10) + "' id='voloRitorno' name='voloRitorno' onclick='prezzoRitorno=this.value'/> € " + myPrezzo * 10 * 10;
    document.getElementById("economyLightStorage5").innerHTML = s5;
    document.getElementById("economyLightStorage6").innerHTML = s5;
    document.getElementById("economyLightStorage7").innerHTML = s5;
    document.getElementById("economyLightStorage8").innerHTML = s5;

    var s6 = "<input type='radio' value='" + (myPrezzo * 15 * 10) + "' id='voloAndata' name='voloAndata' onclick='prezzoAndata=this.value'/> € " + myPrezzo * 15 * 10;
    document.getElementById("economyClassicStorage1").innerHTML = s6;
    document.getElementById("economyClassicStorage2").innerHTML = s6;
    document.getElementById("economyClassicStorage3").innerHTML = s6;
    document.getElementById("economyClassicStorage4").innerHTML = s6;
    var s7 = "<input type='radio' value='" + (myPrezzo * 15 * 10) + "' id='voloRitorno' name='voloRitorno' onclick='prezzoRitorno=this.value'/> € " + myPrezzo * 15 * 10;
    document.getElementById("economyClassicStorage5").innerHTML = s7;
    document.getElementById("economyClassicStorage6").innerHTML = s7;
    document.getElementById("economyClassicStorage7").innerHTML = s7;
    document.getElementById("economyClassicStorage8").innerHTML = s7;

    var s8 = "<input type='radio' value='" + (myPrezzo * 20 * 10) + "' id='voloAndata' name='voloAndata' onclick='prezzoAndata=this.value'/> € " + myPrezzo * 20 * 10;
    document.getElementById("businessStorage1").innerHTML = s8;
    document.getElementById("businessStorage2").innerHTML = s8;
    document.getElementById("businessStorage3").innerHTML = s8;
    document.getElementById("businessStorage4").innerHTML = s8;
    var s9 = "<input type='radio' value='" + (myPrezzo * 20 * 10) + "' id='voloRitorno' name='voloRitorno' onclick='prezzoRitorno=this.value'/> € " + myPrezzo * 20 * 10;
    document.getElementById("businessStorage5").innerHTML = s9;
    document.getElementById("businessStorage6").innerHTML = s9;
    document.getElementById("businessStorage7").innerHTML = s9;
    document.getElementById("businessStorage8").innerHTML = s9;
    return true;
}

// PER voloAndata.html
function inserisciViaggioAndata() {

    var u = JSON.parse(sessionStorage.viaggio);
    var o = {
        partenza: document.getElementsByName("partenza1")[0].value,
        destinazione: document.getElementsByName("destinazione1")[0].value,
        datapartenza: document.getElementsByName("tripstart")[0].value,
        numeroadulti: document.getElementsByName("numadulti1")[0].value,
        numeroteen: document.getElementsByName("numteen1")[0].value,
        numerobimbi: document.getElementsByName("numbimbi1")[0].value,
        numeroneonati: document.getElementsByName("numneonati1")[0].value,
    };


    u[0] = o;
    sessionStorage.viaggio = JSON.stringify(u);
    return true;
}


// PER voloAndataRitorno.html
function inserisciViaggioRitorno() {


    var u = JSON.parse(sessionStorage.viaggio);
    var o = {
        partenza: document.getElementsByName("partenza2")[0].value,
        destinazione: document.getElementsByName("destinazione2")[0].value,
        datapartenza: document.getElementsByName("tripstart1")[0].value,
        dataritorno: document.getElementsByName("tripstart2")[0].value,
        numeroadulti: document.getElementsByName("numadulti2")[0].value,
        numeroteen: document.getElementsByName("numteen2")[0].value,
        numerobimbi: document.getElementsByName("numbimbi2")[0].value,
        numeroneonati: document.getElementsByName("numneonati2")[0].value,
    };


    u[0] = o;
    sessionStorage.viaggio = JSON.stringify(u);
    return true;
}


// funzione per passare la form nella pagina di pagamento


function stampaFormDatiUtenti() {
    var numero_biglietti = numeroBiglietti();
    var s = new String("");
    for (i = 0; i < numero_biglietti; i++) {
        s += "Passeggero " + (i * 1 + 1) + "<br>"
        s += "<div class='form-row'>"
        s += "<div class='col-lg-4 mb-4'>  Nome: <input type='text' name='nome' class='form-control' placeholder='Nome' required onchange='return validaNome();'></input></div>"
        s += "<div class='col-lg-4 mb-4'> Cognome:<input type='text' name='cognome' class='form-control' placeholder='Cognome' required onchange='return validaCognome();'></input></div>"
        s += "<div class='col-lg-4 mb-4'> Data di nascita: <input type='date' class='form-control' name='datanascita' value='' min='1910-01-01' onchange='return mycheckDate();'> </input> </div></div>"
    }

    document.getElementById("datipasseggeri").innerHTML = s;
}
