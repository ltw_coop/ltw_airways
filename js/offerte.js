
function offerteSpeciali(numero) {
    var biglietti = "numbiglietti" + numero
    var u = JSON.parse(sessionStorage.viaggio);
    var o = {
        numeroadulti: document.getElementsByName(biglietti)[0].value,
        numeroteen: 0,
        numerobimbi: 0,
        numeroneonati: 0,
    };
    u[0] = o;
    sessionStorage.viaggio = JSON.stringify(u);
    return true;
}

function controlliOfferte(numero, max) {
    var biglietti = document.getElementsByName("numbiglietti" + numero)[0].value;
    if (biglietti > max) {
        msg("Hai inserito troppi passeggeri");
        return false;
    }
    else if (checkNumber(biglietti) && biglietti > 0) {
        window.location.href = 'form_pagamento.html';
        return true;
    }
    else {
        msg("Hai inserito 0 passeggeri");
        return false;
    }
}