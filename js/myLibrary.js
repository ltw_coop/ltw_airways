function  checkString(Stringa){
    if(Stringa=="") return false;
    else if (!isNaN(Stringa)) return false;
    else return true;
}
function msg(messaggio) {
	alert(messaggio);
}

function checkNumber(numero){
    if(numero=="") return false;
    else if(isNaN(numero)) false;
    else return true;
}

function checkEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


function checkAereoporti(partenza,destinazione){
    if(partenza==destinazione){
        return false;
    }
    else if(destinazione=="nessuna"){
        return false;
    }
    else return true;
}

function checkDate(data){
    var today=new Date();
    //var today=d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate();
    if(new Date(data).getTime() <= today.getTime()){
        return false;
    }
    else return true;
}

function checkPasseggeri(p1,p2,p3,p4){
    if(p1=="" && p2=="" && p3=="" && p4==""){
        return false;
    }
    else if((p1+p2+p3+p4)==0){
        return false;
    }
    else{
        return true;
    }
}

function checkPhoneNumber(numero){
    var check = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    return check.test(numero);
}

function blocca(valore){
        $("#invalidCheck"+valore).prop('checked', false); 
        if(!$("#button"+valore).hasClass("disabled")){
            $("#button"+valore).addClass("disabled");
        }
}