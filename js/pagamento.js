
//controllo passeggeri
function validaNome() {
    if (checkString(document.getElementsByName("nome")[0].value)) {
        return true;
    } else {
        blocca(3);
        msg("Nome non valido");
        return false;
    }
}

function validaCognome() {
    if (checkString(document.getElementsByName("cognome")[0].value)) {
        return true;
    } else {
        blocca(3);
        msg("Cognome non valido");
        return false;
    }

}
function mycheckDate() {
    if (!checkDate(document.getElementsByName("datanascita")[0].value)) {
        return true;
    }
    else {
        blocca(3);
        msg("Data non valida");
        return false;
    }
}

function mycheckPasseggeri() {
    return (validaNome() && validaCognome() && mycheckDate());
}

// controllo contatti passeggero 1

function mycheckEmail() {
    if (checkEmail(document.getElementsByName("email")[0].value)) {
        return true;
    }
    else {
        blocca(3);
        msg("email non valida");
        return false;
    }
}

function validaTelefono() {
    if (checkPhoneNumber(document.getElementsByName("telefono")[0].value)) {
        return true;
    }
    else {
        blocca(3);
        msg("Telefono non valido");
        return false;
    }
}

function validaIndirizzo() {
    if (checkString(document.getElementsByName("indirizzo")[0].value)) {
        return true;
    } else {
        blocca(3);
        msg("Indirizzo non valido");
        return false;
    }
}

function validaCitta() {
    if (checkString(document.getElementsByName("città")[0].value)) {
        return true;
    } else {
        blocca(3);
        msg("Città non valida");
        return false;
    }
}

function validaCAP() {
    if (/^([0-9]{5})$/.test(document.getElementsByName("cap")[0].value)) {
        return true;
    } else {
        blocca(3);
        msg("Cap non valido");
        return false;
    }

}

function validaPaese() {
    if (checkString(document.getElementsByName("paese")[0].value)) {
        return true;
    } else {
        blocca(3);
        msg("Paese non valido");
        return false;
    }
}

function checkContatto() {
    return (mycheckEmail() && validaTelefono() && validaIndirizzo() && validaCitta() && validaCAP() && validaPaese());
}

// controlli pagamento


function validaMese() {
    if (document.getElementsByName("mesecarta")[0].value == "MM") {
        blocca(3);
        msg("Selezionare un mese di scadenza");
        return false;
    } else return true;
}

function validaAnno() {
    if (document.getElementsByName("annocarta")[0].value == "YY") {
        blocca(3);
        msg("Selezionare un anno di scadenza");
        return false;
    } else return true;
}

function validaNumeroCarta() {
    if (checkNumber(document.getElementsByName("numcarta")[0].value)) {
        return true;
    } else {
        blocca(3);
        return false;
    }
}

function validaTitolare() {
    if (checkString(document.getElementsByName("titolare")[0].value)) {
        return true;
    } else {
        blocca(3);
        return false;
    }
}

function validaCVC() {
    if(/^([0-9]{3})$/.test(document.getElementsByName("cvc")[0].value)) {
        return true;
    } else {
        blocca(3);
        return false;
    }
}

// per simulare la sicurezza del pagamento il controllo dei dati è più generico rispetto agli altri
function checkPagamento() {
    if (validaMese() && validaAnno() && validaNumeroCarta() && validaTitolare() && validaCVC()) {
        return true;
    } else {
        blocca(3);
        msg("Dati di pagamento errati");
        return false;
    }
}

// funzione invio finale

function totalcheck3() {
    if (mycheckPasseggeri() && checkContatto() && checkPagamento()) {
        if ($("#button3").hasClass("disabled")) {
            $("#button3").removeClass("disabled");
        }
        return true;
    } else {
        blocca(3);
        return false;
    }

}

// animazione finale

function invia() {
        $("#centralModalSuccess").modal();
}