### Studenti:
    Andrea Asta
    Alessandro Appolloni

-------------------

### Tema progetto:
L'idea del progetto è realizzare un sito web che gestisce prenotazioni di voli per una compagnia aerea. 
Nel sito è possibile prenotare voli di andata e andata/ritorno tra i più importanti aeroporti italiani. 
Sono presenti inoltre una pagine con varie offerte e una pagina di supporto.

### Link del sito:

https://ltw_coop.gitlab.io/ltw_airways